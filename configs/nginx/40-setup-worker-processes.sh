#!/bin/sh
# vim:sw=2:ts=2:sts=2:et

set -eu

LC_ALL=C
ME=$(basename "$0")
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

[ "${NGINX_SET_WORKERS:-}" ] || exit 0

sed -i "s/^\(\s*worker_processes\s*\).*$/\1${NGINX_SET_WORKERS};/" /etc/nginx/nginx.conf
exit 0