ARG BASE_IMAGE
FROM ${BASE_IMAGE:-ubuntu:24.04}
ARG CI_COMMIT_SHA=''
ARG PYVER='3.11'

LABEL maintainer="onegreyonewhite@mail.ru"

# Install node modules
ENV DEBIAN_FRONTEND='noninteractive'

RUN --mount=type=cache,sharing=locked,target=/var/cache/apt \
    --mount=type=cache,sharing=locked,target=/var/lib/apt \
    --mount=type=cache,sharing=locked,target=/root/.cache \
    --mount=type=cache,sharing=locked,target=/root/.pip \
    if [ "$PYVER" = "3.13" ] || [ "$PYVER" = "3.11" ]; then \
    apt update && \
    apt -y install --no-install-recommends --no-upgrade --no-install-suggests \
      software-properties-common \
      gpg-agent \
      gpg && \
    add-apt-repository ppa:deadsnakes/ppa -y; \
    fi && \
    apt update && \
    # Install main packages
    apt -y install --no-install-recommends --no-upgrade --no-install-suggests \
        python$PYVER-minimal \
        libpython$PYVER \
        curl \
        libmysqlclient21 \
        libpq5 \
        libyaml-0-2 \
        ca-certificates \
        &&\
    # install python-pip and project \
    rm -fv /usr/lib/python$PYVER/EXTERNALLY-MANAGED && \
    update-alternatives --install /usr/bin/python python /usr/bin/python$PYVER 3 && \
    curl https://bootstrap.pypa.io/get-pip.py | python$PYVER && \
    python$PYVER -c 'import ssl' && \
    python$PYVER -m pip install -U wheel setuptools && \
    # Cleaning caches and logs \
    if [ "$PYVER" = "3.13" ] || [ "$PYVER" = "3.11" ]; then \
    apt -y remove \
      software-properties-common \
      gpg-agent \
      gpg && \
    apt autoremove -y; \
    fi && \
    rm -rf /tmp/* \
           /var/tmp/* \
           /var/dpkg.log \
           /var/log/apt/* && \
    find . -regex '.*\(*.pyc\|__pycache__\).*' -delete | echo -ne

ENTRYPOINT []
CMD [ "python" ]