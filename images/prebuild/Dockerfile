ARG BASE_IMAGE
FROM ${BASE_IMAGE:-ubuntu:20.04}
ARG CI_COMMIT_SHA=''

LABEL maintainer="onegreyonewhite@mail.ru"

# Install node modules
ENV DEBIAN_FRONTEND='noninteractive'

RUN --mount=type=cache,sharing=locked,target=/var/cache/apt \
    --mount=type=cache,sharing=locked,target=/var/lib/apt \
    --mount=type=cache,sharing=locked,target=/root/.cache \
    --mount=type=cache,sharing=locked,target=/root/.pip \
    apt update && \
    apt -y install --no-install-recommends \
        python3.8-dev \
        libpython3.8 \
        python3-distutils \
        libpcre3 \
        libpcre3-dev \
        curl \
        gcc \
        default-libmysqlclient-dev \
        libmysqlclient21 \
        libyaml-dev \
        pkg-config \
        libpq-dev \
        libpq5 \
        libyaml-0-2 \
        python3-pip \
        &&\
    # install python-pip and project
    python3.8 -m pip install -U pip wheel && \
    python3.8 -m pip install -U \
        mysqlclient~=2.1.0 \
        uWSGI==2.0.21 \
        django-redshift-backend[psycopg2] \
        'django~=3.2.16' \
        && \
    # Cleaning caches and logs
    apt remove -y \
        python3.8-dev \
        libpcre3-dev \
        libyaml-dev \
        default-libmysqlclient-dev \
        libpq-dev \
        gcc \
        && \
    apt autoremove -y && \
    rm -rf /tmp/* \
           /var/tmp/* \
           /var/log/apt/*

ENTRYPOINT []
CMD [ "python3.8" ]
